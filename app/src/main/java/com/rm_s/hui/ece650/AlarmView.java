package com.rm_s.hui.ece650;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.rm_s.hui.ece650.AlarmContract.AlarmDbHelper;


public class AlarmView extends LinearLayout {

    private AlarmViewAdapter adapter;
    private List<AlarmData> l_ad = new ArrayList<>();

    public AlarmView(Context context) {
        super(context);
    }

    public AlarmView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AlarmView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        Button btnAddAlarm;
        ListView lvAlarmList;

        btnAddAlarm = (Button) findViewById(R.id.btnAddAlarm);
        lvAlarmList = (ListView) findViewById(R.id.lvAlarmList);


        adapter = new AlarmViewAdapter(getContext(), android.R.layout.simple_list_item_1, l_ad);
        lvAlarmList.setAdapter(adapter);

        getAlarms(getContext());

        btnAddAlarm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addAlarm();
            }
        });

        lvAlarmList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(getContext()).setTitle("setting").setItems(new CharSequence[]{"Delete"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                deleteAlarm(position);

                                break;
                            default:
                                break;
                        }

                    }
                }).setNegativeButton("Cancel", null).show();

                return true;

            }
        });
    }

    private void addAlarm() {

        Calendar c = Calendar.getInstance();

        new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener(){
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                Calendar currentTime = Calendar.getInstance();
                if (calendar.getTimeInMillis() <= currentTime.getTimeInMillis()) {
                    calendar.setTimeInMillis(calendar.getTimeInMillis()+24*60*60*1000);
                }

                AlarmData ad = new AlarmData(calendar.getTimeInMillis());
                adapter.add(ad);
                ad.save(getContext());
            }
        }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false).show();
    }

    //has deleted , but still ringing.
    private void deleteAlarm(int position) {
        AlarmData ad = adapter.getItem(position);
        adapter.remove(ad);
        ad.delete(getContext());
    }

    private void getAlarms(Context context) {
        AlarmContract ac;
        AlarmDbHelper aDbHelper;
        SQLiteDatabase db;

        ac =  new AlarmContract();
        aDbHelper = ac.new AlarmDbHelper(context);
        db = aDbHelper.getReadableDatabase();

        String[] projection = {
                AlarmContract.Alarms._ID,
                AlarmContract.Alarms.COLUMN_NAME_LABEL,
                AlarmContract.Alarms.COLUMN_NAME_TIME,
                AlarmContract.Alarms.COLUMN_NAME_STATE
        };

        Cursor c = db.query(
                AlarmContract.Alarms.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );

        c.moveToFirst();

        while (!c.isAfterLast()) {
            long alarmId = c.getLong(
                    c.getColumnIndexOrThrow(AlarmContract.Alarms._ID)
            );
            String label = c.getString(
                    c.getColumnIndexOrThrow(AlarmContract.Alarms.COLUMN_NAME_LABEL)
            );
            long time = c.getLong(
                    c.getColumnIndexOrThrow(AlarmContract.Alarms.COLUMN_NAME_TIME)
            );
            int state = c.getInt(
                    c.getColumnIndexOrThrow(AlarmContract.Alarms.COLUMN_NAME_STATE)
            );
            adapter.add(new AlarmData(alarmId, label, time, state));

            c.moveToNext();
        }

        c.close();

    }

}
