package com.rm_s.hui.ece650;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import java.util.Calendar;
import java.util.List;

public class AlarmViewAdapter extends ArrayAdapter<AlarmData>{

    private LayoutInflater layoutInflater;
    private List<AlarmData> l_ad;

    public AlarmViewAdapter(Context context, int resource, List<AlarmData> objects) {
        super(context, resource, objects);
        this.l_ad = objects;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public final class Components {
        public Switch sw_state;
        public TextView tv_time;
    }

    @Override
    public int getCount() { return l_ad.size(); }

    @Override
    public AlarmData getItem(int position) { return l_ad.get(position); }

    @Override
    public long getItemId(int position) { return position; }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        AlarmData ad;
        Calendar ca;

        Components components;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.widget_alarm, null);
            components = new Components();
            components.sw_state = (Switch) convertView.findViewById(R.id.sw_state);
            components.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
            convertView.setTag(components);
        } else {
            components = (Components)convertView.getTag();
        }

        ad = l_ad.get(position);

        ca = Calendar.getInstance();
        ca.setTimeInMillis(ad.getTime());

        // bind data.
        components.tv_time.setText(String.format("%d-%d %d:%d",
                ca.get(Calendar.MONTH)+1,
                ca.get(Calendar.DAY_OF_MONTH),
                ca.get(Calendar.HOUR_OF_DAY),
                ca.get(Calendar.MINUTE)));

        components.sw_state.setChecked(ad.getState());


        //set switch listener
        components.sw_state.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (position >= 0){
                    System.out.println("position: " + position);
                    AlarmData ad = l_ad.get(position);
                    System.out.println("switch id: " + ad.getId());

                    if (b) {
                        ad.turnOn(getContext());

                    } else {
                        ad.turnOff(getContext());
                    }
                }
            }
        });

        return convertView;
    }

}
