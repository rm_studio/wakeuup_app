package com.rm_s.hui.ece650;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public final class AlarmContract {

    public AlarmContract(){}

    public static abstract class Alarms implements BaseColumns {
        public static final String TABLE_NAME = "alarms";
        public static final String COLUMN_NAME_LABEL = "label";
        public static final String COLUMN_NAME_TIME = "time";
        public static final String COLUMN_NAME_SUNDAY = "sunday";
        public static final String COLUMN_NAME_MONDAY = "monday";
        public static final String COLUMN_NAME_TUESDAY = "tuesday";
        public static final String COLUMN_NAME_WEDNESDAY = "wednesday";
        public static final String COLUMN_NAME_THURSDAY = "thursday";
        public static final String COLUMN_NAME_FRIDAY = "friday";
        public static final String COLUMN_NAME_SATURDAY = "saturday";
        public static final String COLUMN_NAME_VIBRATE = "vibrate";
        public static final String COLUMN_NAME_METHOD = "method";
        public static final String COLUMN_NAME_STATE = "state";
        public static final String COLUMN_NAME_NULLABLE = "null";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ALARMS =
            "CREATE TABLE " + Alarms.TABLE_NAME + "(" +
                    Alarms._ID + " INTEGER PRIMARY KEY," +
                    Alarms.COLUMN_NAME_LABEL + TEXT_TYPE + COMMA_SEP +
                    Alarms.COLUMN_NAME_TIME + TEXT_TYPE + COMMA_SEP +
                    Alarms.COLUMN_NAME_SUNDAY + INTEGER_TYPE + COMMA_SEP +
                    Alarms.COLUMN_NAME_MONDAY + INTEGER_TYPE + COMMA_SEP +
                    Alarms.COLUMN_NAME_TUESDAY + INTEGER_TYPE + COMMA_SEP +
                    Alarms.COLUMN_NAME_WEDNESDAY + INTEGER_TYPE + COMMA_SEP +
                    Alarms.COLUMN_NAME_THURSDAY + INTEGER_TYPE + COMMA_SEP +
                    Alarms.COLUMN_NAME_FRIDAY + INTEGER_TYPE + COMMA_SEP +
                    Alarms.COLUMN_NAME_SATURDAY + INTEGER_TYPE + COMMA_SEP +
                    Alarms.COLUMN_NAME_METHOD + INTEGER_TYPE + COMMA_SEP +
                    Alarms.COLUMN_NAME_STATE + INTEGER_TYPE +
                    ")";

    private static final String SQL_DELETE_ALARMS =
            "DROP TABLE IF EXISTS" + Alarms.TABLE_NAME;


    public class AlarmDbHelper extends SQLiteOpenHelper {
        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "Alarms.db";


        public AlarmDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);

        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ALARMS);
            System.out.println("createTables.");

        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_DELETE_ALARMS);
            onCreate(db);
        }

        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }
}


