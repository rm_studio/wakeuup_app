package com.rm_s.hui.ece650;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public final class QuestionContract {

    public QuestionContract(){}

    public static abstract class Questions implements BaseColumns {
        public static final String TABLE_NAME = "questions";
        public static final String COLUMN_NAME_QUESTIONS = "question";
        public static final String COLUMN_NAME_ANSWER_A = "a";
        public static final String COLUMN_NAME_ANSWER_B = "b";
        public static final String COLUMN_NAME_ANSWER_C = "c";
        public static final String COLUMN_NAME_ANSWER_D = "d";
        public static final String COLUMN_NAME_CORRECT_ANSWER = "answer";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_Questions =
            "CREATE TABLE " + Questions.TABLE_NAME + "(" +
                    Questions._ID + " INTEGER PRIMARY KEY," +
                    Questions.COLUMN_NAME_QUESTIONS + TEXT_TYPE + COMMA_SEP +
                    Questions.COLUMN_NAME_ANSWER_A + TEXT_TYPE + COMMA_SEP +
                    Questions.COLUMN_NAME_ANSWER_B + TEXT_TYPE + COMMA_SEP +
                    Questions.COLUMN_NAME_ANSWER_C + TEXT_TYPE + COMMA_SEP +
                    Questions.COLUMN_NAME_ANSWER_D + TEXT_TYPE + COMMA_SEP +
                    Questions.COLUMN_NAME_CORRECT_ANSWER + TEXT_TYPE +
                    ")";

    private static final String SQL_DELETE_Questions =
            "DROP TABLE IF EXISTS" + Questions.TABLE_NAME;


    public class QuestionDbHelper extends SQLiteOpenHelper {
        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "Questions.db";


        public QuestionDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);

        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_Questions);
            System.out.println("createTables.");

        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_DELETE_Questions);
            onCreate(db);
        }

        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }
}
