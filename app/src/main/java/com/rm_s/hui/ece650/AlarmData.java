package com.rm_s.hui.ece650;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;

import java.util.Calendar;

public class AlarmData {

    private static final int STATE_ON = 1;
    private static final int STATE_OFF = 0;
    private static final int VIBRATE_ON = 1;
    private static final int VIBRATE_OFF = 0;
    private static final int DATE_ON = 1;
    private static final int DATE_OFF = 0;
    private static final int METHOD_NORMAL = 0;
    private static final int METHOD_QUESTION = 1;
    private static final int METHOD_MAZE = 2;
    private static final String DEFAULT_LABEL = "Label";

    private long id;
    private String label = "";
    private long time = 0;
    private int Sunday;
    private int Monday;
    private int Tuesday;
    private int Wednesday;
    private int Thursday;
    private int Friday;
    private int Saturday;
    private int vibrate;
    private int method;
    private int state;

    private SQLiteDatabase db;
    private AlarmManager am;

    public AlarmData(long time) {
        this.label = DEFAULT_LABEL;
        this.time = time;
        this.Sunday = DATE_OFF;
        this.Monday = DATE_OFF;
        this.Tuesday = DATE_OFF;
        this.Wednesday = DATE_OFF;
        this.Thursday = DATE_OFF;
        this.Friday = DATE_OFF;
        this.Saturday = DATE_OFF;
        this.vibrate = VIBRATE_OFF;
        this.method = METHOD_NORMAL;
        this.state = STATE_ON;

    }

    public AlarmData(long id, String label, long time, int state) {
        this.id = id;
        this.label = label;
        this.time = time;
        this.vibrate = VIBRATE_OFF;
        this.method = METHOD_NORMAL;
        this.state = state;
    }

    public int getId() {
        return (int) this.id;
    }

    public String getLabel() {
        return label;
    }

    public long getTime() {
        return time;
    }

    public boolean getState() {
        return this.state == 1;
    }


    public String getTimeLabel() {
        String timeLabel;
        Calendar calendar;
        calendar = Calendar.getInstance();
        calendar.setTimeInMillis(this.time);
        timeLabel = String.format("%d-%d %d:%d",
                calendar.get(Calendar.MONTH)+1,
                calendar.get(Calendar.DAY_OF_MONTH),
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE));
        return timeLabel;
    }

    @Override
    public String toString() {
        return getTimeLabel();
    }

    public void save(Context context) {
        initDb(context);

        ContentValues values = new ContentValues();
        values.put(AlarmContract.Alarms.COLUMN_NAME_LABEL, label);
        values.put(AlarmContract.Alarms.COLUMN_NAME_TIME, time);
        values.put(AlarmContract.Alarms.COLUMN_NAME_SUNDAY, Sunday);
        values.put(AlarmContract.Alarms.COLUMN_NAME_MONDAY, Monday);
        values.put(AlarmContract.Alarms.COLUMN_NAME_TUESDAY, Tuesday);
        values.put(AlarmContract.Alarms.COLUMN_NAME_WEDNESDAY, Wednesday);
        values.put(AlarmContract.Alarms.COLUMN_NAME_THURSDAY, Thursday);
        values.put(AlarmContract.Alarms.COLUMN_NAME_FRIDAY, Friday);
        values.put(AlarmContract.Alarms.COLUMN_NAME_SATURDAY, Saturday);
        values.put(AlarmContract.Alarms.COLUMN_NAME_VIBRATE, vibrate);
        values.put(AlarmContract.Alarms.COLUMN_NAME_METHOD, method);

        this.id = db.insert(AlarmContract.Alarms.TABLE_NAME, AlarmContract.Alarms.COLUMN_NAME_NULLABLE, values);

        setAlarm(context);

        System.out.println("save time:" + this.getTimeLabel());


    }

    public void delete(Context context){
        initDb(context);

        String selection = AlarmContract.Alarms._ID + " = " + this.id;

        db.delete(AlarmContract.Alarms.TABLE_NAME, selection, null);

        cancelAlarm(context);

        System.out.println("delete time:" + this.getTimeLabel());

    }

    public int changeState(Context context, int state){
        initDb(context);

        ContentValues values = new ContentValues();
        values.put(AlarmContract.Alarms.COLUMN_NAME_STATE, state);

        String selection = AlarmContract.Alarms._ID + " = " + this.id;

        int count = db.update(
                AlarmContract.Alarms.TABLE_NAME,
                values,
                selection,
                null
        );

        return count;
    }

    public int changeLabel(Context context, String label){
        initDb(context);

        ContentValues values = new ContentValues();
        values.put(AlarmContract.Alarms.COLUMN_NAME_LABEL, label);

        String selection = AlarmContract.Alarms._ID + " = " + this.id;

        int count = db.update(
                AlarmContract.Alarms.TABLE_NAME,
                values,
                selection,
                null
        );

        return count;
    }

    public int changeVibrate(Context context, int vibrate){
        initDb(context);

        ContentValues values = new ContentValues();
        values.put(AlarmContract.Alarms.COLUMN_NAME_VIBRATE, vibrate);

        String selection = AlarmContract.Alarms._ID + " = " + this.id;

        int count = db.update(
                AlarmContract.Alarms.TABLE_NAME,
                values,
                selection,
                null
        );

        return count;
    }

    public int changeMethod(Context context, int method){
        initDb(context);

        ContentValues values = new ContentValues();
        values.put(AlarmContract.Alarms.COLUMN_NAME_METHOD, method);

        String selection = AlarmContract.Alarms._ID + " = " + this.id;

        int count = db.update(
                AlarmContract.Alarms.TABLE_NAME,
                values,
                selection,
                null
        );

        return count;
    }

    //date must be all lowercase.  e.g. sunday
    public int changeDate(Context context, String date, int date_state){
        initDb(context);
        /*
        String date_str = "AlarmContract.Alarms.COLUMN_NAME_"+date;
        System.out.print("change date -> date_str: "+date_str);
        */
        ContentValues values = new ContentValues();
        values.put(date, date_state);

        String selection = AlarmContract.Alarms._ID + " = " + this.id;

        int count = db.update(
                AlarmContract.Alarms.TABLE_NAME,
                values,
                selection,
                null
        );

        return count;
    }

    public void turnOn(Context context){
        this.changeState(context, STATE_ON);
        setAlarm(context);
        System.out.println("turn on time:" + this.getTimeLabel());
    }

    public void turnOff(Context context){
        this.changeState(context, STATE_OFF);
        cancelAlarm(context);
        System.out.println("turn off time:" + this.getTimeLabel());

    }

    public void initDb(Context context) {
        AlarmContract ac;
        AlarmContract.AlarmDbHelper aDbHelper;

        ac =  new AlarmContract();
        aDbHelper = ac.new AlarmDbHelper(context);
        db = aDbHelper.getWritableDatabase();

    }

    private void initAM(Context context) {
        am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

    }

    private void setAlarm(Context context) {
        initAM(context);

        // use intent for passing id.
        Intent i = new Intent(context, AlarmReceiver.class);
        i.putExtra("requestCode", this.getId());

        am.setRepeating(AlarmManager.RTC_WAKEUP,
                this.time,
                5 * 60 * 1000,
                PendingIntent.getBroadcast(context, this.getId(), i, 0));

        System.out.println("set result code: " + this.getId());
    }

    private void cancelAlarm(Context context) {
        initAM(context);

        // use intent for passing id.
        Intent i = new Intent(context, AlarmReceiver.class);
        i.putExtra("requestCode", this.getId());

        am.cancel(PendingIntent.getBroadcast(context, this.getId(), i, 0));

        System.out.println("cancel result code: " + this.getId());
    }
}
