package com.rm_s.hui.ece650;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //why result code always is 0 ?
        /*  resultCode is not the requestCode.
            you can use intent for passing the requestCode/ID.
         */
        //System.out.println("alarm ringing id: " + this.getResultCode() + " ......");

        // try this.
        int requestCode = intent.getIntExtra("requestCode", -1);
        Log.d("ECE650", "alarm ringing id: " + requestCode + " ......");

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(PendingIntent.getBroadcast(context, requestCode, new Intent(context, AlarmReceiver.class), 0));

        Intent i = new Intent(context, PlayAlarmActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }
}
